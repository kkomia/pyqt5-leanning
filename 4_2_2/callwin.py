from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QPushButton
import sys

class winform(QMainWindow):
    def __init__(self, parent=None):
        super(winform, self).__init__(parent)
        
        self.button1 = QPushButton(self)
        self.button1.setText("button")
        self.setWindowTitle("这是一个展示坐标系统的例子")
        self.button1.move(20, 20) # 这是窗口左上角在屏幕中的坐标
        self.resize(300, 200)
        self.move(250, 200)

        print("这是窗口左上角在屏幕中的位置:" + str(self.x()) +", " + str(self.y()))
        print("这是窗口的size:" + str(self.width()) + ", " +str(self.height()))
        print("这是窗口左上角在屏幕中的位置:" + str(self.geometry().x()) +", " + str(self.geometry().y()))
        print("这是窗口的size:" + str(self.geometry().width()) + ", " +str(self.geometry().height()))

if __name__=="__main__":
    app = QApplication(sys.argv)
    win = winform()
    win.show()
    sys.exit(app.exec_())
