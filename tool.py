import os
import os.path

dir = './'

def listUiFile():
    list = []
    files = os.listdir(dir)
    for filename in files:
        if filename.endswith('.ui'):
            list.append(filename)
    return list
    
def transPyFile(filename):
    return os.path.splitext(filename)[0] + '.py'

def main():

    try:
        list = listUiFile()
        for uifile in list:
            pyfile = transPyFile(uifile)
            cmd = 'pyuic5 -o {pyfile} {uifile}'.format(pyfile = pyfile, uifile = uifile)
            os.system(cmd)
    except Exception as ex:
        print("cmd error: " + str(ex))


if __name__=="__main__":
    main()