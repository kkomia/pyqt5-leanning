import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QLineEdit, QWidget, QFormLayout

class lineEditDemo(QWidget):
    def __init__(self, parent = None):
        super(lineEditDemo ,self).__init__(parent)
        self.setWindowTitle("QLineEdit例子")

        layout = QFormLayout()
        pNormalLineEdit = QLineEdit()
        pNoEchoLineEdit = QLineEdit()
        pPasswordLineEdit = QLineEdit()
        pPasswordEchoOnEditLineEdit = QLineEdit()

        layout.addRow("Normal", pNormalLineEdit)
        layout.addRow("NoEcho", pNoEchoLineEdit)
        layout.addRow("Password", pPasswordLineEdit)
        layout.addRow("PasswordEchoOnEdit", pPasswordEchoOnEditLineEdit)

        # setPlaceholderText设置文本框浮现文字
        pNormalLineEdit.setPlaceholderText("Normal")
        pNoEchoLineEdit.setPlaceholderText("NoEcho")
        pPasswordLineEdit.setPlaceholderText("Password")
        pPasswordEchoOnEditLineEdit.setPlaceholderText("PasswordEchoOnEdit")

        # 设置显示效果
        pNormalLineEdit.setEchoMode(QLineEdit.EchoMode.Normal)
        pNoEchoLineEdit.setEchoMode(QLineEdit.EchoMode.NoEcho)
        pPasswordLineEdit.setEchoMode(QLineEdit.EchoMode.Password)
        pPasswordEchoOnEditLineEdit.setEchoMode(QLineEdit.EchoMode.PasswordEchoOnEdit)

        self.setLayout(layout)

if __name__=="__main__":
    app = QApplication(sys.argv)
    win = lineEditDemo()
    win.show()
    sys.exit(app.exec_())

        