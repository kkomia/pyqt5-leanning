import sys
from PyQt5.QtWidgets import QWidget, QApplication, QLineEdit, QFormLayout

class  lineEditDemo(QWidget):
    def __init__(self, parent = None):
        super(lineEditDemo, self).__init__(parent)
        self.setWindowTitle("QlineEdit的输入掩码例子")

        layout = QFormLayout()
        pIPLineEdit = QLineEdit()
        pMACLineEdit = QLineEdit()
        pDateLineEdit = QLineEdit()
        pLicenseLineEdit = QLineEdit()
        

        # 掩码的书写在p145页
        pIPLineEdit.setInputMask("000.000.000.000:_")
        pMACLineEdit.setInputMask("HH:HH:HH:HH:HH:HH;_")
        pDateLineEdit.setInputMask("0000-00-00")
        pLicenseLineEdit.setInputMask(">AAAAA-AAAAA-AAAAA-AAAAA-AAAAA")

        layout.addRow("数字掩码", pIPLineEdit)
        layout.addRow("mac掩码", pMACLineEdit)
        layout.addRow("日期掩码", pDateLineEdit)
        layout.addRow("许可证掩码", pLicenseLineEdit)

        self.setLayout(layout)

if __name__=="__main__":
    app = QApplication(sys.argv)
    win = lineEditDemo()
    win.show()
    sys.exit(app.exec_())