import sys
from PyQt5.QtWidgets import QApplication, QMainWindow

class MainWindow(QMainWindow):
    def __init__(self,parent=None):
        super(MainWindow, self).__init__(parent)
        self.resize(400,200)
        self.status = self.statusBar()
        self.status.showMessage("这是状态栏提示",5000)
        self.setWindowTitle("PyQt MainWindow example")

if __name__=="__main__":
    app = QApplication(sys.argv)
    win = MainWindow()
    win.show()
    sys.exit(app.exec_())