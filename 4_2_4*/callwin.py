import sys
from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QIcon, QPixmap
import apprcc_rc

class  Icon(QWidget):
    def __init__(self, parent = None):
        super(Icon, self).__init__(parent)
        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 250, 150)
        # 这里存在问题 我不知道为什么 QIcon里面的路径填什么都无效的但是他不报错. 图标不显示
        ico = QIcon(QPixmap('./1.ico'))
        self.setWindowIcon(ico)
        self.setWindowTitle("程序图标")


if __name__=="__main__":
    app = QApplication(sys.argv)
    ico = Icon()
    ico.show()
    sys.exit(app.exec_())